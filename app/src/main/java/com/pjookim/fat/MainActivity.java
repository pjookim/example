package com.pjookim.fat;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import java.util.logging.Logger;


public class MainActivity extends ActionBarActivity {

    private EditText mHeightText;
    private EditText mWeightText;
    private TextView mResultView;
    private TextView mFeedBack;
    private Button okButton;
    private RadioGroup radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
            mHeightText = (EditText) findViewById(R.id.height);
            mWeightText = (EditText) findViewById(R.id.weight);
            mResultView = (TextView) findViewById(R.id.text);
            mFeedBack = (TextView) findViewById(R.id.text2);
            okButton = (Button) findViewById(R.id.ok);
            radioGroup = (RadioGroup) findViewById(R.id.rg);

            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    long chText = checkText();
                    if (chText == 0) {
                        double h = Integer.parseInt(mHeightText.getText().toString());
                        double w = Integer.parseInt(mWeightText.getText().toString());
                        double fRate = w / ((h / 100) * (h / 100));

                        String result;
                        String feed;
                        if (fRate <= 18.5) {
                            result = "저체중";
                            feed = "비만보다 저체중이 오히려 안좋습니다!";
                        } else if (fRate <= 23) {
                            result = "정상체중";
                            feed = "건강합니다. 잘 유지하세요!";
                        } else if (fRate <= 25) {
                            result = "과체중";
                            feed = "식사량을 조금만 줄여보세요!";
                        } else if (fRate < 30) {
                            result = "비만";
                            feed = "건강에 좋지 않습니다. 운동량을 늘리세요!";
                        } else {
                            result = "고도비만";
                            feed = "위험합니다. 식사량을 줄이고 운동량을 늘리세요!";
                        }
                        result = "BMI : "+ String.format("%.2f, ", fRate) + result;
                        mResultView.setText(result);
                        mFeedBack.setText(feed);

                    } else if (chText == 1) {
                        SnackbarManager.show(
                                Snackbar.with(MainActivity.this)
                                        .position(Snackbar.SnackbarPosition.TOP)
                                        .text("키와 몸무게를 입력해 주세요.")
                                        .actionLabel("확인")
                                        .actionColor(Color.parseColor("#ff009688")));
                        return;
                    }
                }
            });

            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    // TODO Auto-generated method stub
                }
            });
        }

    public int checkText() {
        int len_h = mHeightText.length();
        int len_w = mWeightText.length();
        String str = "0123456789";
        if(len_h>0 && len_w>0){
            for(int i=0; i<len_h;i++){
                if(str.indexOf(mHeightText.getText().toString().codePointAt(i)) == -1){
                    return 2;
                }
            }
            for(int i=0; i<len_w;i++){
                if(str.indexOf(mWeightText.getText().toString().codePointAt(i)) == -1){
                    return 2;
                }
            }
            return 0;
        }
        else{
            return 1;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.pjookim:
                개발자();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void 개발자() {
        Uri uri = Uri.parse("http://pjookim.com");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}
